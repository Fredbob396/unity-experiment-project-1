﻿namespace Assets.Models
{
    public class DialogSpeaker
    {
        public string Id { get; set; }
        public string Portrait { get; set; }
        public string Sound { get; set; }
        public string Font { get; set; }
    }
}
