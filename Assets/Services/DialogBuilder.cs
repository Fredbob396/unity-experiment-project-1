﻿using System.Collections.Generic;
using Assets.Models;

namespace Assets.Services
{
    class DialogBuilder : IDialogBuilder
    {
        private readonly List<DialogMessage> _dialog;
        private DialogSpeaker _speaker;
        private int? _expression;
        private float? _delay;
        private bool? _autoSkip;
        private bool? _noPunctuationDelay;

        // TODO: Get defaults from somewhere?
        public DialogBuilder(
            DialogSpeaker speaker = null,
            int expression = 0, 
            float delay = 0.03f, 
            bool autoSkip = false,
            bool noPunctuationDelay = false)
        {
            _dialog = new List<DialogMessage>();
            _speaker = speaker;
            _expression = expression;
            _delay = delay;
            _autoSkip = autoSkip;
            _noPunctuationDelay = noPunctuationDelay;
        }

        public List<DialogMessage> GetDialog()
        {
            return _dialog;
        }

        /// <summary>
        /// Pushes a line of dialog to the dialog list
        /// </summary>
        public void Add(string text, int? expression = null, DialogSpeaker speaker = null, float? delay = null, bool? autoSkip = null, bool? noPunctuationDelay = null)
        {
            ParseParams(ref speaker, ref expression, ref delay, ref autoSkip, ref noPunctuationDelay);
            _dialog.Add(new DialogMessage
            {
                Text = GetFormattedText(text),
                Speaker = speaker,
                Expression = (int)expression,
                Delay = (float)delay,
                AutoSkip = (bool)autoSkip,
                NoPunctuationDelay = (bool)noPunctuationDelay
            });
        }

        /// <summary>
        /// Clears the dialog list and reset dialog properties
        /// </summary>
        public void Clear()
        {
            _speaker = null;
            _expression = 0;
            _delay = 0.03f;
            _autoSkip = false;
            _noPunctuationDelay = false;
            _dialog.Clear();
        }

        private void ParseParams(ref DialogSpeaker speaker, ref int? expression, ref float? delay, ref bool? autoSkip, ref bool? noPunctuationDelay)
        {
            // Speaker
            if (speaker == null)
                speaker = _speaker;
            else
                _speaker = speaker;

            // Expression
            if (expression == null)
                expression = _expression;
            else
                _expression = expression;

            // Delay
            if (delay == null)
                delay = _delay;
            else
                _delay = delay;

            // Auto Skip
            if (autoSkip == null)
                autoSkip = _autoSkip;
            else
                _autoSkip = (bool)autoSkip;

            // No Punctuation Delay
            if (noPunctuationDelay == null)
                noPunctuationDelay = _noPunctuationDelay;
            else
                _noPunctuationDelay = (bool) noPunctuationDelay;
        }

        private string GetFormattedText(string text)
        {
            string formattedText = "";
            bool afterFirstAst = false;
            for (int index = 0; index < text.Length; index++)
            {
                char c = text[index];
                if (!afterFirstAst && c == '*')
                {
                    formattedText += "*<indent=7%>";
                    afterFirstAst = true;
                }else if (c == '*')
                {
                    formattedText += "</indent>\n*<indent=7%>";
                }
                else
                {
                    formattedText += c;
                }

                // Add end tag if any asterisk found
                if (afterFirstAst && index == text.Length)
                    formattedText += "</indent>";
            }

            return formattedText;
        }
    }
}
