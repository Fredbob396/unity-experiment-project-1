﻿using System.Collections.Generic;
using Assets.Models;

namespace Assets.Services
{
    public interface IDialogBuilder
    {
        List<DialogMessage> GetDialog();
        void Add(string text, int? expression, DialogSpeaker speaker, float? delay, bool? autoSkip, bool? noPunctuationDelay);
        void Clear();
    }
}
