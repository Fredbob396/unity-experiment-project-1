﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Services
{
    class G
    {
        /// <summary>
        /// Pixels-per-unit
        /// </summary>
        public static readonly int PPU = 20;

        /// <summary>
        /// Units-per-pixel
        /// </summary>
        public static readonly double UPP = 0.05f;

        public static readonly int InteractablesLayer = 256;
    }
}
