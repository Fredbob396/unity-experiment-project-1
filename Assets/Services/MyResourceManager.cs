﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Services
{
    /// <summary>
    /// Manages loading and unloading resources
    /// </summary>
    class MyResourceManager
    {
        private readonly Dictionary<string, Object> _singleResources = new Dictionary<string, Object>();
        private readonly Dictionary<string, Object[]> _arrayResources = new Dictionary<string, Object[]>();

        /// <summary>
        /// Get a single Unity resource
        /// </summary>
        public T GetResource<T>(string resourceId) where T : Object
        {
            Object resource;
            // Try to get the resource
            if (_singleResources.TryGetValue(resourceId, out resource))
            {
                //Debug.Log("Retrieved existing resource: " + resourceId);
                return (T)resource;
            }
            // Resource not loaded
            else
            {
                Object newResource = Resources.Load<T>(resourceId);

                // Add loaded resource to dictionary
                if (newResource != null)
                {
                    //Debug.Log("Loaded new resource: " + resourceId);
                    _singleResources.Add(resourceId, newResource);

                    return (T)newResource;
                }
                // No resource found!
                else
                {
                    throw new NullReferenceException("Resource not found!: " + resourceId);
                }
            }
        }

        /// <summary>
        /// Get an array of Unity resources
        /// </summary>
        public T[] GetAllResources<T>(string resourceId) where T : Object
        {
            Object[] resource;
            // Try to get the resource
            if (_arrayResources.TryGetValue(resourceId, out resource))
            {
                //Debug.Log("Retrieved existing resource: " + resourceId);
                return (T[])resource;
            }
            // Resource not loaded
            else
            {
                Object[] newResource = Resources.LoadAll<T>(resourceId);

                // Add loaded resource to dictionary
                if (newResource != null)
                {
                    //Debug.Log("Loaded new resource: " + resourceId);
                    _arrayResources.Add(resourceId, newResource);
        
                    return (T[])newResource;
                }
                // No resource found!
                else
                {
                    throw new NullReferenceException("Resource not found!: " + resourceId);
                }
            }
        }

        /// <summary>
        /// Clears the resource dictionaries and unloads all of it's loaded assets
        /// </summary>
        public void Clear()
        {
            foreach (var value in _singleResources.Select(x => x.Value))
            {
                Resources.UnloadAsset(value);
            }
            foreach (var value in _arrayResources.SelectMany(x => x.Value))
            {
                Resources.UnloadAsset(value);
            }
            _singleResources.Clear();
            _arrayResources.Clear();
        }
    }
}
