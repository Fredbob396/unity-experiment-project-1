﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeController : MonoBehaviour
{
    private Image _overlay;

    // Start is called before the first frame update
    void Start()
    {
        _overlay = gameObject.GetComponent<Image>();
        SetMaxFadeIn();
        StartCoroutine(FadeOut());
    }

    public void SetMaxFadeIn()
    {
        var oCol = _overlay.color;
        oCol.a = 1f;
        _overlay.color = oCol;
    }

    public IEnumerator FadeOut()
    {
        while (_overlay.color.a > 0)
        {
            var oCol = _overlay.color;
            oCol.a -= 0.1f;
            _overlay.color = oCol;
            yield return new WaitForSeconds(0.03f);
        }
        PlayerController.Lock = false;
    }

    public IEnumerator FadeIn()
    {
        PlayerController.Lock = true;
        while (_overlay.color.a < 1)
        {
            var oCol = _overlay.color;
            oCol.a += 0.1f;
            _overlay.color = oCol;
            yield return new WaitForSeconds(0.03f);
        }
    }
}
