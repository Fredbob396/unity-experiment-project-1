﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Services;
using UnityEngine;

public class BattleStartController : MonoBehaviour
{
    public GameObject Player;
    public GameObject Enemy;
    public GameObject BattleOverlay;
    public GameObject AfterImage;

    private MyResourceManager _resourceManager;
    private BattleBackgroundController _battleBackgroundController;
    private PlayerController _playerController;
    private AudioSource _audioSource;
    private Vector3 _playerOriginalPosition;

    private const int ArcFactor = 2;

    // Use this for initialization
    void Awake ()
    {
        _resourceManager = new MyResourceManager();
        _playerController = Player.GetComponent<PlayerController>();
        _audioSource = GetComponent<AudioSource>();
        _battleBackgroundController = BattleOverlay.GetComponent<BattleBackgroundController>();
    }

    public void StartBattle()
    {
        StartCoroutine(StartBattleEnum());
    }

    public void StopBattle()
    {
        StartCoroutine(StopBattleEnum());
    }

    IEnumerator StartBattleEnum()
    {
        PlayerController.Lock = true;
        CameraController.Lock = true;
        yield return StartCoroutine(Encounter());
        _playerController.ChangeState(6);
        BattleOverlay.SetActive(true);
        yield return StartCoroutine(PlayerStartAnimation());
    }

    IEnumerator StopBattleEnum()
    {
        yield return StartCoroutine(PlayerEndAnimation());
        _battleBackgroundController.StartFadeOut();
        _audioSource.Stop();
        CameraController.Lock = false;
    }

    IEnumerator Encounter()
    {
        _playerOriginalPosition = Player.transform.position;
        var tensionHorn = _resourceManager.GetResource<AudioClip>("snd_tensionhorn");
        _audioSource.PlayOneShot(tensionHorn);
        yield return new WaitForSeconds(tensionHorn.length);
    }

    IEnumerator PlayerEndAnimation()
    {
        _playerController.ChangeState(101);
        yield return new WaitForSeconds(1.36f);
        yield return StartCoroutine(BezierTranslateGameObjectToPosition(Player, _playerOriginalPosition, true));
    }

    IEnumerator PlayerStartAnimation()
    {
        var camPos = Camera.main.transform.position;

        // Translate player/enemy to start position
        StartCoroutine(BezierTranslateGameObjectToPosition(Player, new Vector3(camPos.x - 6.5f, camPos.y, camPos.z)));
        yield return StartCoroutine(BezierTranslateGameObjectToPosition(Enemy, new Vector3(camPos.x + 5.5f, camPos.y, camPos.z)));

        // Play start animation + sound
        _playerController.ChangeState(100);
        var startSound = _resourceManager.GetResource<AudioClip>("snd_weaponpull_fast");
        _audioSource.PlayOneShot(startSound);

        // Load music and set clip
        var battleMusic = _resourceManager.GetResource<AudioClip>("battle");
        _audioSource.clip = battleMusic;

        // Wait for animation to finish and then play start music
        yield return new WaitForSeconds(startSound.length - 0.2f);
        PlayerController.Lock = false;
        _audioSource.Play();
    }

    IEnumerator BezierTranslateGameObjectToPosition(GameObject gObject, Vector3 endPoint, bool reversePeakMinPoint = false)
    {
        var p0 = gObject.transform.position;

        // Calculate the peak of the arc
        var peakY = reversePeakMinPoint
            ? Mathf.Clamp(endPoint.y + ArcFactor, p0.y + ArcFactor, Mathf.Infinity)
            : Mathf.Clamp(p0.y + ArcFactor, endPoint.y + ArcFactor, Mathf.Infinity);

        var p1 = new Vector3(p0.x, peakY, p0.z);
        var p2 = new Vector3(endPoint.x, peakY, endPoint.z);
        var p3 = endPoint;

        yield return TranslateAlongBezierCurve(gObject, p0, p1, p2, p3);
    }

    // Translate an object along a bezier curve
    IEnumerator TranslateAlongBezierCurve(GameObject go, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        //int afterImageCounter = 0;
        for (double t = 0; t < 1.4; t += 0.04) // 1.4 because it misses a loop otherwise
        {
            var pos = GetPointOnBezierCurve(p0, p1, p2, p3, (float)t);
            go.transform.position = pos;

            if(go.name.Equals("Player"))
                Instantiate(AfterImage, go.transform.position, go.transform.rotation);

            yield return new WaitForSeconds(0.01f);
        }
    }

    // https://denisrizov.com/2016/06/02/bezier-curves-unity-package-included/
    // https://en.wikipedia.org/wiki/B%C3%A9zier_curve
    Vector3 GetPointOnBezierCurve(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        Vector3 a = Vector3.Lerp(p0, p1, t);
        Vector3 b = Vector3.Lerp(p1, p2, t);
        Vector3 c = Vector3.Lerp(p2, p3, t);

        Vector3 d = Vector3.Lerp(a, b, t);
        Vector3 e = Vector3.Lerp(b, c, t);

        Vector3 pointOnCurve = Vector3.Lerp(d, e, t);

        return pointOnCurve;
    }
}
