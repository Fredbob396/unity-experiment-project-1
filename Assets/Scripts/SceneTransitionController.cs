﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransitionController : MonoBehaviour
{
    public GameObject FadeToBlack;

    private static FadeController _fadeController;

    private static string _destinationSpawnPoint;
    private static int _playerSpawnAnimationState;

    // Use this for initialization
    void Awake ()
    {
        _fadeController = FadeToBlack.GetComponent<FadeController>();
    }

    /// <summary>
    /// Transition to a new scene at a specified spawn point with a specified animation state
    /// </summary>
    public void TransitionToScene(string scene, string spawnPoint, int animState)
    {
        // Save these variables for the SpawnPointController
        _destinationSpawnPoint = spawnPoint;
        _playerSpawnAnimationState = animState;

        StartCoroutine(StartTransition(scene));
    }

    IEnumerator StartTransition(string scene)
    {
        yield return StartCoroutine(_fadeController.FadeIn());
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }

    /// <summary>
    /// Get the destination spawn point
    /// </summary>
    public static string GetDestinationSpawnPoint()
    {
        return _destinationSpawnPoint;
    }

    /// <summary>
    /// Get transition's saved player animation state
    /// </summary>
    public static int GetPlayerSpawnAnimationState()
    {
        return _playerSpawnAnimationState;
    }
}
