﻿using System.Collections;
using UnityEngine;

public class AfterimageController : MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;

    // Use this for initialization
    void Awake ()
    {
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeOut()
    {
        while (_spriteRenderer.color.a > 0)
        {
            var srCol = _spriteRenderer.color;
            srCol.a -= 0.1f;
            _spriteRenderer.color = srCol;
            yield return new WaitForSeconds(0.04f);
        }
        Destroy(gameObject);
    }
}
