﻿using Assets.Services;
using UnityEngine;

[RequireComponent(typeof(InteractableProperties))]
public class ActivatorDummy : MonoBehaviour
{
    public GameObject DialogBox;

    private InteractableProperties _properties;
    private DialogBoxController _dialogBoxController;
    private DialogBuilder _db;
    private int _convoIndex;

    // Grab any components needed by object here
    void Awake()
    {
        // Always grab the interactable properties
        _properties = GetComponent<InteractableProperties>();
        _dialogBoxController = DialogBox.GetComponent<DialogBoxController>();
    }
    
    // Use this for initialization
    void Start () {
        
    }
    
    // Activation logic generally goes here
    void Update () {
        if (!_properties.Activated) return;
        
        switch (_convoIndex)
        {
            case 0:
                _db = new DialogBuilder(Speakers.None);
                _db.Add("*Ralsei, the lonely prince, is now your ally.");
                _db.Add("*The power of fluffy boys shines within you.");
                _convoIndex++;
                break;
            case 1:
                _db = new DialogBuilder(Speakers.Ralsei, DE.RalseiShocked);
                _db.Add("*Oh, Kris!*It's the training dummy I made!");
                _db.Add("*Now seems like a great chance to prepare for the enemy.", DE.RalseiEyesClosedSmiling);
                _db.Add("*Would you like me to teach you how to fight?", DE.RalseiSmiling);
                _db.Add("*Yes. *No.", speaker:Speakers.None);
                _db.Add("*OK!*Get ready, Kris!", speaker:Speakers.Ralsei);
                _convoIndex++;
                break;
            case 2:
                _db = new DialogBuilder(Speakers.Ralsei, DE.RalseiEyesClosedSmiling);
                _db.Add("*Oh, that was fun!*You're a wonderful student, Kris!");
                _db.Add("*... and, er, in case you ever need a refresher, I...", DE.RalseiLookingDown);
                _db.Add("*Here!*I wrote a Manual for you and Susie!", DE.RalseiEyesClosedSmiling);
                _db.Add("*Press [C] to open the menu and use it in your ITEMS.", DE.RalseiSmiling);
                _convoIndex++;
                break;
            case 3:
                _db = new DialogBuilder(Speakers.Ralsei);
                _db.Add("*Do you really think someone would do that, Kris?", DE.RalseiConcernedSmile);
                _db.Add("*Just go on the internet and tell lies?");
                _convoIndex++;
                break;
            case 4:
                _db = new DialogBuilder(Speakers.Lancer);
                _db.Add("*Hey, Toothpaste boy!*I need help with something!", DE.LancerSmileToothy);

                _db.Add("*What do you need help with, Lancer?", speaker: Speakers.Ralsei, expression: DE.RalseiSmiling);

                _db.Add("*I just need you to read this, okay?*Here!", speaker: Speakers.Lancer, expression: DE.LancerSmileTongueOut);

                _db.Add("*Umm... sure! Let me take a look.", speaker: Speakers.Ralsei, expression: DE.RalseiConcernedSmile);
                _db.Add("*...", DE.RalseiLookingDown);
                _db.Add("*...?", DE.RalseiLookingDownConcerned);
                _db.Add("*This is...*Um... what is this, Lancer?", DE.RalseiLookingDownConcernedSmile);

                _db.Add("*Just read it!*Please?", speaker: Speakers.Lancer, expression: DE.LancerSmileTongueOut);

                _db.Add("*Okay, okay, fine.*(Ahem)...", speaker: Speakers.Ralsei, expression: DE.RalseiConcernedSmile);
                _db.Add("*So long suckers!", DE.RalseiAngry);
                _db.Add("*I rev up my motorcycle and create a huge cloud of smoke.");
                _db.Add("*When the cloud dissipates...", DE.RalseiSmiling);
                _db.Add("*I'm lying completely dead on the pavement.", DE.RalseiEyesClosedSmiling);

                _db.Add("*Ha! This is going in my Tik Tok cringe compilation!", speaker: Speakers.Lancer, expression: DE.LancerHandsome);

                _db.Add("*...", speaker: Speakers.Ralsei, expression: DE.RalseiConfused);
                break;
        }

        _dialogBoxController.Dialog = _db.GetDialog();
        DialogBox.SetActive(true);
        _properties.Activated = false;
    }
}
