﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneTransitionZoneController : MonoBehaviour
{
    public GameObject SceneTransitionManager;

    private SceneTransitionController _sceneTransitionController;

    public string DestinationScene;
    public string DestinationSpawnPoint;
    public int PlayerSpawnAnimationState;

    void Awake()
    {
        _sceneTransitionController = SceneTransitionManager.GetComponent<SceneTransitionController>();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name.Equals("Player"))
        {
            _sceneTransitionController.TransitionToScene(DestinationScene, DestinationSpawnPoint, PlayerSpawnAnimationState);
        }
    }
}
