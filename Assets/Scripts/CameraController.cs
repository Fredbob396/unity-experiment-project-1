﻿using System;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject FollowTarget;
    public static bool Lock = false;
    public float MaxX;
    public float MinX;
    public float MaxY;
    public float MinY;

    // TODO: Calculate offset to follow center given top-left anchor
    private const float OffsetX = 0.55f;
    private const float OffsetY = -1.05f;

    private float _frustumWidth;
    private float _frustumHeight;

    // Use this for initialization
    void Start()
    {
        var mainCamera = Camera.main;

        // Get the points on the screen for calculating frustum sizes
        var bottomLeft = mainCamera.ScreenToWorldPoint(new Vector3(0, 0));
        var bottomMiddle = mainCamera.ScreenToWorldPoint(new Vector3(mainCamera.pixelWidth / 2, 0));
        var middleLeft = mainCamera.ScreenToWorldPoint(new Vector3(0, mainCamera.pixelHeight / 2));

        // Calculate frustum sizes based on the distance between points on the screen
        _frustumWidth = Vector2.Distance(bottomLeft, bottomMiddle);
        _frustumHeight = Vector2.Distance(bottomLeft, middleLeft);
    }

    // LateUpdate is called after Update each frame
    void LateUpdate()
    {
        DebugDrawBounds();

        if (Lock) return;

        Vector3 pos = FollowTarget.transform.GetChild(0).position;

        // Move the position respecting the fustrum bounds
        transform.position = new Vector3(
            Mathf.Clamp(pos.x + OffsetX, MinX + _frustumWidth, MaxX - _frustumWidth),
            Mathf.Clamp(pos.y + OffsetY, MinY + _frustumHeight, MaxY - _frustumHeight),
            pos.z);
    }

    /// <summary>
    /// Draw the camera's bounds
    /// </summary>
    void DebugDrawBounds()
    {
        // Left Side
        Debug.DrawLine(new Vector3(MinX, MinY, 1), new Vector3(MinX, MaxY, 1), Color.magenta);

        // Right Side
        Debug.DrawLine(new Vector3(MaxX, MinY, 1), new Vector3(MaxX, MaxY, 1), Color.magenta);

        // Top Side
        Debug.DrawLine(new Vector3(MinX, MaxY, 1), new Vector3(MaxX, MaxY, 1), Color.magenta);

        // Bottom Side
        Debug.DrawLine(new Vector3(MinX, MinY, 1), new Vector3(MaxX, MinY, 1), Color.magenta);
    }
}
