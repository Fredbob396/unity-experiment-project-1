﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnPointController : MonoBehaviour
{
    public GameObject Player;
    public GameObject FallbackSpawnPoint;
    public int FallbackPlayerSpawnAnimationState;

    // Use this for initialization
    void Start ()
    {
        var playerController = Player.GetComponent<PlayerController>();

        // Get all spawnpoint GameObjects
        var spawnPoints = GameObject.Find("SpawnPoints")
            .GetComponentsInChildren<Transform>(true)
            .Select(x => x.gameObject).ToList();

        // Get vars from SceneTransitionController
        var spawnPointId = SceneTransitionController.GetDestinationSpawnPoint();
        var playerAnimState = SceneTransitionController.GetPlayerSpawnAnimationState();

        // If not specified, spawn at specified default
        if (spawnPointId == null || playerAnimState == 0)
        {
            Debug.Log("WARNING: Using fallback spawn point");

            // Move the player to the spawn point
            Player.transform.position = FallbackSpawnPoint.transform.position;

            // Set the player's animation state
            playerController.ChangeState(FallbackPlayerSpawnAnimationState);
        }
        else
        {
            // Find the spawn point
            var spawnPoint = spawnPoints.FirstOrDefault(x => x.name.Equals(spawnPointId));

            // Move the player to the spawn point
            Player.transform.position = spawnPoint.transform.position;

            // Set the player's animation state
            playerController.ChangeState(playerAnimState);
        }

        // Hide all spawnpoints after player spawned
        foreach (var spawnPoint in spawnPoints)
        {
            spawnPoint.SetActive(false);
        }

        // No need for the SpawnPointManager after transition
        gameObject.SetActive(false);
    }
}