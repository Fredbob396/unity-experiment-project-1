﻿using Assets.Services;
using UnityEngine;

// This object serves as en example for how interactables function for the demo
[RequireComponent(typeof(InteractableProperties))]
public class TestObstacleScript : MonoBehaviour
{
    public GameObject DialogBox;

    private DialogBoxController _dialogBoxController;
    private DialogBuilder _db;
    private InteractableProperties _properties;
    private int _convoIndex;

    // Grab any components needed by object here
    void Awake()
    {
        _dialogBoxController = DialogBox.GetComponent<DialogBoxController>();
        _properties = GetComponent<InteractableProperties>();
    }

    // Use this for initialization
    void Start () {
        
    }

    // Activation logic generally goes here
    void Update () {
        if (!_properties.Activated) return;

        switch (_convoIndex)
        {
            case 0:
                _db = new DialogBuilder(Speakers.Ralsei);
                _db.Add("*Now, let's talk about SELECTING COMMANDS.", DE.RalseiSmiling);
                _db.Add("*These ICONS will let you ISSUE COMMANDS to YOUR TEAM.");
                _db.Add("*The first ICON, THE SWORD, is the ATTACK COMMAND.");
                _db.Add("*Using it lets you, um...*Hurt people...", DE.RalseiLookingDown);
                _db.Add("*... well, I guess you have to defend yourself sometimes, right?", DE.RalseiConcernedSmile);
                _convoIndex++;
                break;
            case 1:
                _db = new DialogBuilder(Speakers.Ralsei, DE.RalseiShocked);
                _db.Add("*K-Kris!?");
                _db.Add("*Y-you must have missed...*H... Haha...", DE.RalseiConcernedSmile);
                _convoIndex++;
                break;
            case 2:
                _db = new DialogBuilder(Speakers.Ralsei, DE.RalseiShocked);
                _db.Add("*K-Kris!?*D-did you miss again!?");
                _db.Add("*...", DE.RalseiLookingDown);
                _db.Add("*Though, I mean, if you wanted to hit me, that's ok, too...", DE.RalseiConcernedSmileBlushing);
                _convoIndex++;
                break;
            case 3:
                _db = new DialogBuilder(Speakers.Ralsei, DE.RalseiShocked);
                _db.Add("*...", DE.RalseiLookingDown);
                _db.Add("*Kris, it's um...", DE.RalseiLookingDownConcerned);
                _db.Add("*Really hard for me to teach when you're hitting me.", DE.RalseiLookingDownConcernedSmile);
                _db.Add("*Let's take a break and go find Susie, OK?", DE.RalseiConcernedSmile);
                _convoIndex++;
                break;
            default:
                _db = new DialogBuilder(Speakers.Ralsei, DE.RalseiSmiling);
                _db.Add("*How was that, Kris?*Did I actually sound upset?");
                _db.Add("*I know you'd never ACTUALLY hit me!", DE.RalseiEyesClosedSmiling);
                break;
        }

        _dialogBoxController.Dialog = _db.GetDialog();
        DialogBox.SetActive(true);
        _properties.Activated = false;
    }
}
