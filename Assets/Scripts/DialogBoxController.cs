﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Models;
using Assets.Services;
using TMPro;
using UnityEngine;

public class DialogBoxController : MonoBehaviour
{
    // Public Variables
    public List<DialogMessage> Dialog; // Should be populated before Start()
    public float BasePunctuationDelayFactor = 5.33f;

    // Private Variables
    private readonly char[] _punctuationDelayChars = { ',', ';', '!', '?' };
    private readonly char[] _silentChars = { '*', ' ', ',', ';', '!', '?' };
    private MyResourceManager _resourceManager;

    // Portrait Calculation Constants
    private const float DialogBoxSpriteHeightPixels = 76;   // DialogBox's sprite height in pixels

    // Components
    private TMP_Text _text;
    private TMP_TextInfo _textInfo;
    private SpriteRenderer _portrait;
    private AudioSource _audio;
    private AudioClip _audioClip;

    // Constants
    private readonly Vector4 _portraitMargins = new Vector4(70.0f, 13.0f, 11.0f, 13.0f);
    private readonly Vector4 _noPortraitMargins = new Vector4(15.0f, 13.0f, 11.0f, 13.0f);

    // When first set active
    void Awake()
    {
        _text = GameObject.Find("Dialog Text").GetComponent<TMP_Text>();
        _portrait = GameObject.Find("Portrait").GetComponent<SpriteRenderer>();
        _audio = GetComponent<AudioSource>();
        _textInfo = _text.textInfo;
        _text.color = new Color(0,0,0,0); // Color set here so preview still shows text
        _resourceManager = new MyResourceManager();
    }

    // Should only be activated after Dialog is populated
    void OnEnable()
    {
        // Disable player movement OnEnable
        PlayerController.Lock = true;
        StartCoroutine(PlayMessages());
    }

    void OnDisable()
    {
        // Restore control OnDisable
        PlayerController.Lock = false;
    }

    // Clears all DialogBox resources, usually for next scene
    void OnDestroy()
    {
        _resourceManager.Clear();
    }

    // TODO: FOR TESTING ONLY. REMOVE IN FINAL
    public void TestUnloadAssets()
    {
        Debug.Log("WARNING: Unloading all loaded DialogBox assets!");
        _resourceManager.Clear();
    }

    /// <summary>
    /// Play all messages in the Dialog list
    /// </summary>
    IEnumerator PlayMessages()
    {
        // Play each message one after the other
        foreach (DialogMessage message in Dialog)
        {
            yield return StartCoroutine(PlayMessage(message));
        }

        // After all messages are played, clear Dialog and deactivate DialogBox
        Dialog.Clear();
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Play the specified message
    /// </summary>
    IEnumerator PlayMessage(DialogMessage message)
    {
        // Load message resources
        _text.text = message.Text;
        if (message.Speaker.Portrait != null)
        {
            _text.margin = _portraitMargins;
            _portrait.sprite = _resourceManager.GetAllResources<Sprite>(message.Speaker.Portrait)[message.Expression];
        }
        else
        {
            _text.margin = _noPortraitMargins;
            _portrait.sprite = null;
        }
        
        _audioClip = _resourceManager.GetResource<AudioClip>(message.Speaker.Sound);

        AdjustPortrait();

        /* BUGFIX
         * Immediately yield to allow TextMesh to update with new text */
        yield return null;

        bool done = false;

        var periodDelayIndexes = GetPeriodDelayIndexes(_textInfo.characterInfo.Select(x => x.character).ToArray());

        // For all visible characters
        for (int charIndex = 0; charIndex < _textInfo.characterCount; charIndex++)
        {
            // If the user presses z at any point, enter skip mode
            bool skip = Input.GetKey("x");

            float punctuationDelayFactor = 1.0f;

            char currentChar = _textInfo.characterInfo[charIndex].character;

            // If punctuation delay is allowed
            if (!message.NoPunctuationDelay)
            {
                // Apply text delay based on punctuation characters
                if (_punctuationDelayChars.Contains(currentChar) || periodDelayIndexes.Contains(charIndex))
                    punctuationDelayFactor += BasePunctuationDelayFactor;
            }

            // Only change the vertex color and play sound if the character is visible.
            if (_textInfo.characterInfo[charIndex].isVisible)
            {
                // Color the character white
                ColorTMPCharacter(charIndex, Color.white);

                // Don't play sound when skipping (Saves headphone users)
                // Don't play sound if silent char
                if (!skip && !_silentChars.Contains(currentChar))
                    _audio.PlayOneShot(_audioClip);
            }

            // Don't apply talk delay if skipping
            if (!skip)
                yield return new WaitForSeconds(message.Delay * punctuationDelayFactor);
        }

        // Prevent auto-close when skipping
        yield return null;

        // End dialog when z is pressed
        while (!done)
        {
            // Using GetKeyDown to make it a distinct z press from the skip z
            if (Input.GetKeyDown("z") || message.AutoSkip)
                done = true;
            else
                yield return null;
        }
    }

    /// <summary>
    /// Get the indexes of single characters or ends of ellipsis for applying delays
    /// </summary>
    int[] GetPeriodDelayIndexes(char[] message)
    {
        // Get all period indexes
        var periodIndexes = Enumerable.Range(0, message.Length)
            .Where(i => message[i] == '.')
            .ToList();

        var delayIndexes = new List<int>();

        //TODO: Convert to for loop?
        foreach (int periodIndex in periodIndexes)
        {
            var lCheck = periodIndex - 1;
            var rCheck = periodIndex + 1;

            // Only check within bounds of the array
            if (lCheck >= 0 && rCheck <= message.Length)
            {
                var leftNeighbor = message[lCheck];
                var rightNeighbor = message[rCheck];

                // Single period
                if (leftNeighbor != '.' && rightNeighbor != '.')
                    delayIndexes.Add(periodIndex);

                // End of ellipsis
                if (leftNeighbor == '.' && rightNeighbor != '.')
                    delayIndexes.Add(periodIndex);
            }
        }
        return delayIndexes.ToArray();
    }

    /// <summary>
    /// Colors the TextMesh Pro character at the specified index
    /// </summary>
    void ColorTMPCharacter(int charIndex, Color32 color)
    {
        // Get the index of the material used by the current character.
        int materialIndex = _textInfo.characterInfo[charIndex].materialReferenceIndex;

        // Get the vertex colors of the mesh used by this text element (character or sprite).
        Color32[] newVertexColors = _textInfo.meshInfo[materialIndex].colors32;

        // Get the index of the first vertex used by this text element.
        int vertexIndex = _textInfo.characterInfo[charIndex].vertexIndex;

        newVertexColors[vertexIndex + 0] = color;
        newVertexColors[vertexIndex + 1] = color;
        newVertexColors[vertexIndex + 2] = color;
        newVertexColors[vertexIndex + 3] = color;

        _text.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);
    }

    /// <summary>
    /// Adjusts the character portrait to be centered within the DialogBox
    /// </summary>
    void AdjustPortrait()
    {
        // Get the original vector
        Vector3 localPos = _portrait.gameObject.transform.localPosition;

        // Calculate the loaded portrait's height in pixels
        float portraitHeightPixels = _portrait.bounds.size.y * 20;

        // Calculate the Y-value needed to place the portrait in the center of the DialogBox
        double newLocalY = -Mathf.Floor((DialogBoxSpriteHeightPixels - portraitHeightPixels) / 2) * G.UPP;

        _portrait.gameObject.transform.localPosition = new Vector3(localPos.x, (float)newLocalY, localPos.z);
    }
}