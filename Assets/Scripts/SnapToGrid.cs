﻿using Assets.Services;
using UnityEngine;

public class SnapToGrid : MonoBehaviour
{
    private void Update()
    {
        Vector3 position = transform.localPosition;

        position.x = (Mathf.Round(transform.parent.position.x * G.PPU) / G.PPU) - transform.parent.position.x;
        position.y = (Mathf.Round(transform.parent.position.y * G.PPU) / G.PPU) - transform.parent.position.y;

        transform.localPosition = position;
    }
}