﻿using System.Collections;
using System.Collections.Generic;
using Assets.Services;
using UnityEngine;

/// <summary>
/// Controls the battle background
/// </summary>
public class BattleBackgroundController : MonoBehaviour
{
    private const float BgSpriteHeight = 50f;
    private float _maxOffset;
    private ScrollerContainer _blackBg;
    private ScrollerContainer _leftScroller;
    private ScrollerContainer _rightScroller;
    private ScrollerContainer[] _allScrollerContainers;

    void Awake()
    {
        var cSpriteRenderers = gameObject.GetComponentsInChildren<SpriteRenderer>();

        _blackBg = new ScrollerContainer
        {
            SpriteRenderer = cSpriteRenderers[0],
            GameObject = cSpriteRenderers[0].gameObject,
            OriginalLocalPosition = cSpriteRenderers[0].gameObject.transform.localPosition
        };

        _rightScroller = new ScrollerContainer
        {
            SpriteRenderer = cSpriteRenderers[1],
            GameObject = cSpriteRenderers[1].gameObject,
            OriginalLocalPosition = cSpriteRenderers[1].gameObject.transform.localPosition
        };

        _leftScroller = new ScrollerContainer
        {
            SpriteRenderer = cSpriteRenderers[2],
            GameObject = cSpriteRenderers[2].gameObject,
            OriginalLocalPosition = cSpriteRenderers[2].gameObject.transform.localPosition
        };

        _allScrollerContainers = new[]
        {
            _blackBg,
            _leftScroller,
            _rightScroller
        };

        _maxOffset = BgSpriteHeight / G.PPU;
    }

    void OnEnable()
    {
        // Set max fade then fade in
        SetMaxFadeOut();
        StartCoroutine(FadeIn());
    }

    void Update ()
    {
        // Scroll the left and right scrollers
        ScrollScroller(_leftScroller, -0.5f, 0.5f);
        ScrollScroller(_rightScroller, 0.5f, -0.5f);
    }

    void LateUpdate()
    {
        // Reset position if reached max offset
        foreach (var c in _allScrollerContainers)
        {
            if (c.TotalXOffset > _maxOffset || c.TotalYOffset > _maxOffset)
            {
                c.GameObject.transform.localPosition = c.OriginalLocalPosition;
                c.TotalXOffset = 0.0f;
                c.TotalYOffset = 0.0f;
            }
        }
    }

    /// <summary>
    /// Scroll a scroller with the given offsets
    /// </summary>
    void ScrollScroller(ScrollerContainer scroller, float xOffset, float yOffset)
    {
        xOffset = xOffset * Time.deltaTime;
        yOffset = yOffset * Time.deltaTime;

        // Get and adjust position
        var position = scroller.GameObject.transform.localPosition;
        position.x += xOffset;
        position.y += yOffset;

        // Update offset tracker
        scroller.TotalXOffset += xOffset;
        scroller.TotalYOffset += yOffset;

        // Apply transform
        scroller.GameObject.transform.localPosition = position;
    }

    /// <summary>
    /// Fade out background
    /// </summary>
    public void StartFadeOut()
    {
        SetMaxFadeIn();
        StartCoroutine(FadeOut());
    }

    /// <summary>
    /// Set all background components completely transparent
    /// </summary>
    private void SetMaxFadeOut()
    {
        var transparent = new Color(255,255,255,0);
        foreach (ScrollerContainer c in _allScrollerContainers)
        {
            c.SpriteRenderer.color = transparent;
        }
    }

    /// <summary>
    /// Set all background components completely opaque
    /// </summary>
    private void SetMaxFadeIn()
    {
        var opaque = new Color(255, 255, 255, 1);
        foreach (ScrollerContainer c in _allScrollerContainers)
        {
            c.SpriteRenderer.color = opaque;
        }
    }

    /// <summary>
    /// Fade all background components in
    /// </summary>
    private IEnumerator FadeIn()
    {
        var alphaTracker = 0.0f;
        while (alphaTracker < 1)
        {
            foreach (var c in _allScrollerContainers)
            {
                var color = c.SpriteRenderer.color;
                color.a += 0.025f;

                c.SpriteRenderer.color = color;
            }
            alphaTracker += 0.025f;
            yield return new WaitForSeconds(0.005f);
        }
    }

    /// <summary>
    /// Fade all background components out
    /// </summary>
    private IEnumerator FadeOut()
    {
        var alphaTracker = 1.0f;
        while (alphaTracker > 0)
        {
            foreach (var c in _allScrollerContainers)
            {
                var color = c.SpriteRenderer.color;
                color.a -= 0.025f;

                c.SpriteRenderer.color = color;
            }
            alphaTracker -= 0.025f;
            yield return new WaitForSeconds(0.005f);
        }
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Contains scroller information
    /// </summary>
    class ScrollerContainer
    {
        public GameObject GameObject;
        public SpriteRenderer SpriteRenderer;
        public Vector3 OriginalLocalPosition;
        public float TotalXOffset;
        public float TotalYOffset;
    }
}
