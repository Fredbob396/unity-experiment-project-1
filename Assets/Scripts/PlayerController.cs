﻿using System;
using System.Linq;
using Assets.Services;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float Speed = 0.5f;
    public static bool Lock = false;
    public GameObject GameManager;

    private int _currentAnimationState;
    private int _direction = 2;
    private Animator _anim;
    private Rigidbody2D _rigidbody;
    private RaycastHit2D _hit;

    // Track previous translate to prevent staying in place when pressing opposite direction
    private float _prevX;
    private float _prevY;

    // Idle Animations
    private const int AnimIdleLeft = 4;
    private const int AnimIdleRight = 6;
    private const int AnimIdleUp = 8;
    private const int AnimIdleDown = 2;

    // Walk Animations
    private const int AnimWalkLeft = 40;
    private const int AnimWalkRight = 60;
    private const int AnimWalkUp = 80;
    private const int AnimWalkDown = 20;

    private const float RunSpeedFactor = 1.5f;

    // TEST VARIABLES
    private BattleStartController _bsController;

    void Awake()
    {
        _anim = GetComponentInChildren<Animator>();
        _rigidbody = GetComponent<Rigidbody2D>();
        _bsController = GameManager.GetComponent<BattleStartController>();
    }

    void FixedUpdate()
    {
        // Disable controls and stop moving if locked
        if (Lock)
        {
            ManageAnimation(0,0);
            return;
        }

        // Get running or normal speed
        float speed;
        if (Input.GetKey("x"))
        {
            speed = Speed * RunSpeedFactor;
            _anim.speed = RunSpeedFactor;
        }
        else
        {
            speed = Speed;
            _anim.speed = 1;
        }


        float x = 0.0f;
        float y = 0.0f;

        // Move left if not moved right previous frame
        if (Input.GetKey("left") && _prevX <= 0)
        {
            x -= speed * Time.deltaTime;
        }
        // Move right if not moved left previous frame
        if (Input.GetKey("right") && _prevX >= 0)
        {
            x += speed * Time.deltaTime;
        }
        // Move up if not moved down previous frame
        if (Input.GetKey("up") && _prevY >= 0)
        {
            y += speed * Time.deltaTime;
        }
        // Move down in not moved up previous frame
        if (Input.GetKey("down") && _prevY <= 0)
        {
            y -= speed * Time.deltaTime;
        }
        // Handle activated object
        if (_hit.transform != null && Input.GetKeyDown("z"))
        {
            // Get the hit object
            var hitObject = _hit.transform.gameObject;

            // See if it has an interactable properties script
            var test = hitObject.GetComponent<InteractableProperties>();

            // If it does, set Activated to true.
            // This is the trigger for interactable objects
            if (test != null)
                test.Activated = true;
        }

        // Test start battle
        if (Input.GetKeyDown("m"))
        {
            _bsController.StartBattle();
        }

        // Test end battle
        if (Input.GetKeyDown("n"))
        {
            _bsController.StopBattle();
        }

        ManageAnimation(x, y);
        _rigidbody.MovePosition(new Vector2(_rigidbody.position.x + x, _rigidbody.position.y + y));
        

        // Save previous translate
        _prevX = x;
        _prevY = y;
    }

    void LateUpdate()
    {
        // Don't bother raycasting if locked
        if (Lock)
            return;

        // Do raycast and save result to local var
        _hit = RaycastFromPlayer();
    }

    /// <summary>
    /// Handle animating the character based on the x and y movement values
    /// </summary>
    void ManageAnimation(float x, float y)
    {
        // Handle Stopping
        if (x == 0 && y == 0)
        {
            switch (_currentAnimationState)
            {
                case AnimWalkLeft:
                    ChangeState(AnimIdleLeft);
                    break;
                case AnimWalkRight:
                    ChangeState(AnimIdleRight);
                    break;
                case AnimWalkUp:
                    ChangeState(AnimIdleUp);
                    break;
                case AnimWalkDown:
                    ChangeState(AnimIdleDown);
                    break;
            }
        }
        // Handle Changing Direction
        else
        {
            // If there's no horizontal movement
            if (x == 0)
            {
                // There must be vertical movement, so walk up or down depending on y
                if (y > 0)
                {
                    ChangeState(AnimWalkUp);
                    _direction = 8;
                }
                    
                else if (y < 0)
                {
                    ChangeState(AnimWalkDown);
                    _direction = 2;
                }
            }

            // If there's no vertical movement
            if (y == 0)
            {
                // There must be horizontal movement, so walk left or right depending on x
                if (x > 0)
                {
                    ChangeState(AnimWalkRight);
                    _direction = 6;
                }
                else if (x < 0)
                {
                    ChangeState(AnimWalkLeft);
                    _direction = 4;
                }
            }

            // If there is movement on both axes and the character is not walking horizontally
            if ((Math.Abs(y) > 0 && Math.Abs(x) > 0) && !AnimationWalkingHorizontal())
            {
                // Use the walk up or down animation for diagonal movement
                if (y > 0)
                {
                    ChangeState(AnimWalkUp);
                    _direction = 8;
                }
                else if (y < 0)
                {
                    ChangeState(AnimWalkDown);
                    _direction = 2;
                }
            }
        }
    }

    private RaycastHit2D RaycastFromPlayer()
    {
        Vector2 vector;
        var x = transform.position.x;
        var y = transform.position.y;

        switch (_direction)
        {
            case 8:
                vector = Vector2.up;
                x += 0.55f;
                break;
            case 2:
                vector = Vector2.down;
                x += 0.55f;
                y -= 2.15f;
                break;
            case 4:
                vector = Vector2.left;
                y -= 1.075f;
                break;
            case 6:
                vector = Vector2.right;
                x += 1.15f;
                y -= 1.075f;
                break;
            default:
                vector = Vector2.down;
                break;
        }

        var adjustedPosition = new Vector3(x,y);

        RaycastHit2D hit = Physics2D.Raycast(adjustedPosition, vector, 0.85f, G.InteractablesLayer);

        if(hit.transform != null)
            Debug.DrawLine(adjustedPosition, hit.point, Color.red);

        return hit;
    }

    /// <summary>
    /// Check if the player's animation state is walking left or right
    /// </summary>
    bool AnimationWalkingHorizontal()
    {
        return _currentAnimationState == AnimWalkLeft || _currentAnimationState == AnimWalkRight;
    }

    /// <summary>
    /// Change the player's animation state to a different state
    /// </summary>
    public void ChangeState(int state)
    {
        if (_currentAnimationState == state)
        {
            return;
        }

        _anim.SetInteger("State", state);
        _currentAnimationState = state;
    }
}
